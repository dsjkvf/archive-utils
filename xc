#!/usr/bin/env bash

case $2 in
    '')
        [ -z $1 ] && echo "ERROR: No archive name was prvided" || echo "ERROR: No file names were prvided"
        exit 1
        ;;
esac

case "$1" in
    *.tar.gz|*.tgz|*.tar)
        if tar --version | grep -iq gnu
        then
            tarc='tar czvf'
            tarr='tar cWvf'
        else
            tarc='tar -czvf'
            tarr='tar -cvf'
        fi
        ;;
esac

case "$1" in
    *.tar.gz|*.tgz)
        name=$1
        shift
        $tarc "$name" "$@"
        ;;
    *.tar)
        name=$1
        shift
        $tarr "$name" "$@"
        ;;
    *.zip|*.crx|*.epub)
        name=$1
        shift
        zip -r "$name" "$@"
        ;;
    *.7z)
        name=$(basename "$1" .7z)
        shift
        7z a -t7z -r -mx9 -mhe=on -p "$name" "$@"
        ;;
    *.dmg)
        if uname | grep -iq darwin
        then
            name=$1
            shift
            fold=$(mktemp -d)
            cp -X "$@" $fold/
            read -p "Enter SIZE: " size
            hdiutil create -encryption -stdinpass -fs HFS+J -volname $name -size $size -srcfolder $fold/ $name -attach
            rm -rf $fold/
        else
            echo "ERROR: Creating a DMG archive is only supported on macOS"
            exit 1
        fi
        ;;
    *)
        echo "ERROR: Only TAR, ZIP, 7Z and DMG archives are supported"
        exit 1
        ;;
esac
